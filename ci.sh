#!/bin/bash

if [ -z "${1}" ]; then
    source ./get-versions.sh
    echo "Using ${LATEST_VERSION} as the Jenkins version number"
    JENKINS_VERSION=${LATEST_VERSION}
else
   JENKINS_VERSION=${1}
fi 

rm -rf jci-docker
git clone https://github.com/jenkinsci/docker jci-docker

#Remove the volume line
cat 'jci-docker/Dockerfile' | sed 's/VOLUME \/var\/jenkins_home/# No Volume for you/g' > jci-docker/Dockerfile.novolume

# Get the SHA for our build
JENKINS_SHA=`wget -qO- https://repo.jenkins-ci.org/public/org/jenkins-ci/main/jenkins-war/${JENKINS_VERSION}/jenkins-war-${JENKINS_VERSION}.war.sha256`

echo "Jenkins SHA should be ${JENKINS_SHA}"

DOCKER_TAG_BASE="${DOCKERHUB_USER}/${DOCKER_PROJECT}"

DOCKER_TAG="${DOCKER_TAG_BASE}:${JENKINS_VERSION}"

echo "Now attempting to build ${DOCKER_TAG}"


docker build -t ${DOCKER_TAG} --build-arg JENKINS_VERSION=${JENKINS_VERSION} --build-arg JENKINS_SHA=${JENKINS_SHA} -f jci-docker/Dockerfile.novolume jci-docker/


if [ -n "${DOCKERHUB_PASS}" ]; then
	docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASS $DOCKERHUB_SERVER
	docker push ${DOCKER_TAG}
	
	if [ "${JENKINS_VERSION}" = "${LATEST_VERSION}" ]; then
		docker tag ${DOCKER_TAG} ${DOCKER_TAG_BASE}
	fi
fi
