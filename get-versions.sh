#!/bin/bash
curl -s "https://jenkins.io/changelog/" | grep -o 'new in \(.*\)' | cut -d " " -f 3 > versions.txt
LATEST_VERSION=`cat versions.txt | head -n 1`

echo "${LATEST_VERSION} is the most recent build of Jenkins"
